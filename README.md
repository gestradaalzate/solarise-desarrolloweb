# SolaRise-DesarrolloWeb

DEFINICION DEL CONTEXTO DE NEGOCIO:

Propósito ➫
La empresa SolaRise está enfocada en la implementación de energías renovables a través de la comercialización de diferentes productos, y la prestación de servicios que buscan transformar la vida de sus usuarios y a su vez generar un impacto positivo a nivel ambiental. 

PRODUCTOS:

Páneles Solares ➫
SolaRise se encarga de proveer una variedad de productos de alta calidad para crear un ambiente ameno en los hogares que decidan formar parte de este ambicioso proyecto. Nuestra empresa está enfocada en la implementación de sistemas de páneles solares, los cuales están diseñados para aprovechar al máximo la energía solar. Utilizamos tecnologías de avanzada en páneles fotovoltaicos, los cuales tienen la capacidad de abastecer todo el hogar con independencia energética.

Baterias ➫ 
La energía producida durante el día por los páneles, es utilizada en las necesidades del hogar. No obstante, el excedente de la misma se puede almacenar en baterías LG de alta calidad, para que las necesidades nocturnas, cuando el sol se haya puesto, puedan ser satisfechas. Adicionalmente, el remanente puede ser vendido a las empresas de energía locales, resultando en una disminución importante del costo de la facturación mensual por parte de ellos.


SERVICIOS:

Estudio de viabilidad ➫
En SolaRise te brindamos una asesoría completa y evaluamos la viabilidad de acceso a nuestros servicios según las necesidades, con el fin de ofrecer una variedad de planes de financiación según sea el caso.  

Instalación ➫
Efectuamos servicio de instalación de páneles solares en el hogar sin costos adicionales y garantizamos su correcta funcionalidad a largo plazo.

Mantenimiento de páneles solares ➫
Una vez efectuada la instalación, y transcurridos 3 meses, nos encargamos de efectuar el primer mantenimiento en los páneles solares. Teniendo en cuenta que la periodicidad de estos son de tres a cuatro veces al año, el beneficiario puede programar las visitas posteriores a muy bajo costo. Esto con el fin de garantizar la calidad y el buen funcionamiento de nuestros productos.
