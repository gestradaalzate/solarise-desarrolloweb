const mongoose=require("mongoose");

/**
 * It connects to the database using the mongoose.connect() method, and then it logs a message to the
 * console
 */
const connectDatabase=()=>{
  mongoose.connect(process.env.DB_LOCAL_URI,{useNewUrlParser:true,
  useUnifiedTopology:true}).then(con=>{
    console.log(`Base de datos mongo conectada con el servidor: ${con.connection.host}`);
  })
}

const db =()=> mongoose.connection;
module.exports={connectDatabase, db};

