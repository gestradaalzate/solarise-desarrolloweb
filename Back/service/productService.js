const{createProduct,getAllProducts,getOneProductById,deleteProduct,updateProduct}=require('../db/ProductDao');

/**
 * This function is used to get all the products from the database
 * @param req - The request object.
 * @param res - The response object.
 */
const getAllProductsService=async (req,res)=>{
  const getAllProduct=req.params;
  getAllProducts(getAllProduct).then((getProducts)=> res.send(getProducts));
}

/**
 * This function is used to get one product by id
 * @param req - The request object.
 * @param res - The response object.
 */
const getOneProductByIdService= async(req,res)=>{
  getOneProductById(req.params.id).then((getProduct)=>{
    (!getProduct)? res.status(404).json({
        message:"Product not found"}):res.send(getProduct)
      })
  }

/**
 * It takes the request body, passes it to the createProduct function, and then sends the response back
 * to the client
 * @param req - The request object. This object represents the HTTP request and has properties for the
 * request query string, parameters, body, HTTP headers, and so on.
 * @param res - The response object.
 */
const createProductService= async(req,res)=>{
  createProduct(req.body).then((productCreated) => {
    res.send(productCreated);
  })
}

/**
 * It deletes a product from the database and returns a message if the product was deleted or not
 * @param req - The request object. This contains information about the HTTP request that raised the
 * event.
 * @param res - The response object.
 */
const deleteProductService= async(req,res)=>{
  deleteProduct(req.params.id).then((productDeleted)=>{
    (!productDeleted)? res.status(404).json({
        message: "Product not found"}):res.send("Product deleted")
      })
}


/**
 * It takes the id of the product to be updated from the request parameters, and the updated product
 * data from the request body, and then it calls the updateProduct function from the productService.js
 * file, and then it sends a response to the client
 * @param req - The request object. This contains information about the HTTP request that raised the
 * event.
 * @param res - The response object.
 */
const updateProductService= async(req,res)=>{
  updateProduct(req.params.id,req.body).then((productUpdated)=>{
     console.log(productUpdated)
     res.send('Update one product')
  })
}


module.exports={
  getAllProductsService,
  getOneProductByIdService,
  createProductService,
  deleteProductService,
  updateProductService}