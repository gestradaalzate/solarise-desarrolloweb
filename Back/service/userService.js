const { createUser, getAll, getOneById, deleteUser, updateUser, deleteUserByEmailDao } = require('../db/userDao');

/**
 * This function is an async function that takes in a request and a response, and then it takes the
 * request parameters and passes them into the getAll function, which returns a promise that is then
 * sent back to the client
 * @param req - The request object.
 * @param res - The response object.
 */
const getAllService = async (req, res) => {
    const getAllUsers = req.params;
    getAll(getAllUsers).then((usersGet) => res.send(usersGet));
}

/**
 * This function is used to get one user by id
 * @param req - The request object. This contains information about the HTTP request that raised the
 * event.
 * @param res - The response object.
 */
const getOneUserByIdService = async (req, res) => {
    const id = req.params.id
    getOneById(id).then((getUser) => {
        (!getUser) ? res.status(404).json({ message: "User no found" }) : res.send(getUser)
    })
}

/**
 * It takes the request body, passes it to the createUser function, and then sends the response
 * @param req - the request object
 * @param res - the response object
 */
const createUserService = async (req, res) => {
    const userToBeCreate = req.body
    createUser(userToBeCreate).then((userCreated) => {
        res.send(userCreated);
    })
}


/**
 * It deletes a user from the database and returns a message if the user was deleted or not
 * @param req - The request object. This contains information about the HTTP request that raised the
 * event.
 * @param res - The response object.
 */
const deleteUserService = async (req, res) => {
    deleteUser(req.params.id).then((userDeleted) => {
        (!userDeleted) ?
            (res.status(404).json({ message: "user no found" })) : (res.send("user Deleted"))
    })
}

const deleteUserByEmailService = async (req, res) => {
    deleteUserByEmailDao(req.params.email).then((userDeleted) => {
        (!userDeleted) ?
            (res.status(404).json({ message: "user no found" })) : (res.send("user Deleted"))
    })
}

/**
 * It takes the id of the user to be updated from the request parameters, and the updated user
 * information from the request body, and then updates the user in the database
 * @param req - The request object. This contains information about the HTTP request that raised the
 * event.
 * @param res - The response object.
 */
const updatedUserService = async (req, res) => {
    updateUser(req.params.id, req.body).then((userupdated) => {
        console.log(userupdated)
        res.send("User updated ")
    })
}

module.exports = {
    getAllService,
    getOneUserByIdService,
    createUserService,
    deleteUserService,
    deleteUserByEmailService,
    updatedUserService
}