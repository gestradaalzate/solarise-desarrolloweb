const { createTransaction, getOneTransactionById, deleteTransaction, getAllTransactions, updateTransaction } = require("../db/transactionsDao")


const getAllTransactionsService = async (req, res) => {
  const getAllTransaction = req.params;
  getAllTransactions(getAllTransaction).then((getTransactions) => res.send(getTransactions));
}

const getOneTransactionByIdService = async (req, res) => {
  getOneTransactionById(req.params.id).then((getTransaction) => {
    (!getTransaction) ? res.status(404).json({
      message: "Transaction no found"
    }) : res.send(getTransaction)
  })
}


const createTransactionService = async (req, res) => {
  createTransaction(req.body).then((transactionCreated) => {
    res.send(transactionCreated);
  })
}

const deleteTransactionService = async (req, res) => {
  deleteTransaction(req.params.id).then((transactionDeleted) => {
    (!transactionDeleted) ? res.status(404).json({
      message: "Transaction no found"
    }) : res.send("Transaction Deleted")
  })
}

const updateTransactionService = async (req, res) => {
  updateTransaction(req.params.id, req.body).then((transactionUpdated) => {
    console.log(transactionUpdated)
    res.send('Updated one Transaction')
  })
}


module.exports = {
  getAllTransactionsService,
  getOneTransactionByIdService,
  createTransactionService,
  deleteTransactionService,
  updateTransactionService
}