const express = require('express')
var routerUser = express.Router();
const {
  getAllService,
  getOneUserByIdService,
  deleteUserByEmailService,
  createUserService,
  deleteUserService,
  updatedUserService } = require('../service/userService')

const name = '/api/v1/users'

/* This is a route that will be called when the user makes a GET request to the
`/api/v1/users/healthCheck` endpoint. */
routerUser.get(name + '/healthCheck', (req, res) => {
  res.send('Users healthCheck')
})

/* A route that will be called when the user makes a GET request to the `/api/v1/users/all` endpoint. */
routerUser.get(name + '/all', (req, res) => getAllService(req, res))

/* This is a route that will be called when the user makes a GET request to the `/api/v1/users/one/:id`
endpoint. */
routerUser.get(name +'/one/:id',(req,res)=>
getOneUserByIdService(req,res))

/* This is a route that will be called when the user makes a POST request to the `/api/v1/users/`
endpoint. */
routerUser.post(name + '/', (req, res) => createUserService(req, res))

/* This is a route that will be called when the user makes a DELETE request to the `/api/v1/users/:id`
endpoint. */
routerUser.delete(name + '/:id', (req, res) => deleteUserService(req, res))


routerUser.delete(name + '/email/:email', (req, res) => deleteUserByEmailService(req, res))


/* This is a route that will be called when the user makes a POST request to the `/api/v1/users/`
endpoint. */
routerUser.put(name + '/:id', (req, res) => updatedUserService(req, res))

module.exports = { routerUser }