const express = require('express')
var productRouter = express.Router();
const {
  createProductService, 
  deleteProductService, 
  getOneProductByIdService,
  getAllProductsService,
  updateProductService}=require('../service/productService')

/* A constant that is used to define the route. */
const name = '/api/v1/products'

/* This is a health check endpoint. It is used to check if the service is up and running. */
productRouter.get(name + '/healthCheck', (req, res) => res.send('Product healthCheck'))

/* This is a route handler. It is a function that is called when a request is made to the specified
route. */
productRouter.get(name + '/all',(req, res) => getAllProductsService(req,res))

/* This is a route handler. It is a function that is called when a request is made to the specified
route. */
productRouter.get(name + '/one/:id',(req, res) => getOneProductByIdService(req,res) )

/* Creating a new product. */
productRouter.post(name + '/',(req, res) => createProductService(req,res))

/* Deleting a product by id. */
productRouter.delete(name + '/:id',(req, res) => deleteProductService(req,res)) 

/* Updating a product by id. */
productRouter.put(name + '/:id', (req, res)=> updateProductService(req,res))


module.exports={productRouter}