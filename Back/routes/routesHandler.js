const {routerUser} = require('./userRoutes');
const {productRouter} = require('./productRoutes');
const {authRoter} = require('./authRoutes')
const passport= require('passport');
const {routerTest} = require('./testRoutes')
const{routerTransaction}=require("./transactionRoutes")

function routesHandler(app) {
  app.use(routerTest);
  app.use(authRoter);
  app.use(routerUser);
  app.use(routerTransaction)
  app.use(passport.authenticate('jwt', { session: false }),productRouter);

}

module.exports = { routesHandler }