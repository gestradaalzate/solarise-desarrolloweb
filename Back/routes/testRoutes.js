const express = require('express')
const routerTest = express.Router();

routerTest.get( '/healthCheck', (req, res) => {
    res.send('Test healthCheck')
  })

module.exports = {routerTest}