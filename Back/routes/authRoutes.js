const express = require('express')
const authRoter = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

require('../middelware/Auth')

/* This is the route for signup. */
authRoter.post(
    '/signup',
    passport.authenticate('signup', { session: false }),
    async (req, res, next) => {
        res.json({
            message: 'Signup successful',
            user: req.user
        });
    }
);

/* This is the route for login. */
authRoter.post('/login', async (req, res, next) => {
    passport.authenticate(
        'login',
        async (err, user, info) => {
            try {
                if (err || !user) {
                    const error = new Error('An error occurred.');
                    return next(error);
                }
                const token = jwt.sign({ user: user }, 'TOP_SECRET_PASSWORD_LETTER_JUST_TEST');
                return res.json({ token });
            } catch (error) {
                return next(error);
            }
        })(req, res, next);
},(req, res, next)=>{
    console.log(res)
    console.log(next)
});
module.exports = { authRoter }