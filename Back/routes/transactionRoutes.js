const express = require('express')
var routerTransaction = express.Router();
const {
  getAllTransactionsService,
  getOneTransactionByIdService,
  createTransactionService,
  deleteTransactionService,
  updateTransactionService } = require('../service/transactionService')

const name = '/api/v1/transactions'

/* This is a health check endpoint. */
routerTransaction.get(name + '/healthCheck', (req, res) => res.send('Transaction healthCheck'))

/* This is a route that will be used to get all transactions. */
routerTransaction.get(name + '/all', (req, res) => getAllTransactionsService(req, res))

/* This is a route that will be used to get one transaction by id. */
routerTransaction.get(name + '/one/:id', (req, res) =>getOneTransactionByIdService(req, res))

/* This is a route that will be used to create a transaction. */
routerTransaction.post(name + '/', (req, res) => createTransactionService(req, res))

/* This is a route that will be used to delete a transaction by id. */
routerTransaction.delete(name + '/:id', (req, res) => deleteTransactionService(req, res))

/* This is a route that will be used to update a transaction by id. */
routerTransaction.put(name + '/:id', (req, res) => updateTransactionService(req, res))


module.exports = { routerTransaction: routerTransaction }