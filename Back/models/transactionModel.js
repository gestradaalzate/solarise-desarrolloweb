const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const transactionSchema = new Schema({
  transactionID: ObjectId,
  address:String,
  city:String,
  numberPhone:String,
  state:String,
  date:{
    type: Date,
    default:Date.now},
  userId:{
    type:mongoose.Schema.Types.ObjectId,
    required:true,
    ref:"Users"
  },
  totalPrice: Number,
});

const transactionModel = mongoose.model('Transactions', transactionSchema)
module.exports = transactionModel;