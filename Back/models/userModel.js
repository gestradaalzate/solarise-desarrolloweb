const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

/* This is creating a new schema for the user model. */
const UsersSchema = new Schema({
  userId: ObjectId,
  userName: String,
  userLastName: String,
  userPassword: String,
  userEmail: String,
  userRole: String,
  image: String
});

/* This is a method that is being added to the UsersSchema. It is a function that takes a password as a
parameter. It then compares the password to the user's password. */
UsersSchema.methods.isValidPassword = async function(password) {
  console.log("🚀 ~ file: userModel.js ~ line 19 ~ UsersSchema.methods.isValidPassword=function ~ password", password)
  const user = this;
  const compare = await bcrypt.compare(password, user.userPassword);
  return compare;
}

const UserModel = mongoose.model("Users", UsersSchema);

module.exports = UserModel