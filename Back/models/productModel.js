const mongoose=require('mongoose')
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

/* Creating a schema for the product model. */
const productSchema = new Schema({
  productID: ObjectId,
  productName: String,
  productPrice: Number,
  userId: String,
  productAvaiability: Boolean,
  productStock: Number,
  productDescription: String,
  image: String
});

const productModel=mongoose.model('Products',productSchema)
module.exports= productModel