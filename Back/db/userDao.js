const User = require('../models/userModel')
const bcrypt = require('bcrypt');
/* Creating a new user. */
var createUser = async (userToBeCreated) => {
    console.log("🚀 ~ file: userDao.js ~ line 13 ~ createUser ~ userToBeCreated", userToBeCreated)
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(userToBeCreated.userPassword, salt);
    const userSaved = new User({
        userPassword: password,
        userName: userToBeCreated.userName,
        userLastName: userToBeCreated.userLastName,
        userEmail: userToBeCreated.userEmail,
        userRole: userToBeCreated.userRole,
        image: userToBeCreated.image
    })
        .save();
    console.log("🚀 ~ file: userDao.js ~ line 12 ~ createUser ~ userSaved", userSaved)
    return userSaved;
}


/**
 * It finds a user by id, and if it finds one, it deletes it
 * @param id - The id of the user you want to delete.
 * @returns The userDeleted is being returned.
 */
const deleteUser = async (id) => {
    const userDeleted = await User.findById({ _id: id })
    if (userDeleted) {
        console.log("Deleted one user")
        return userDeleted.remove()
    }
};

const deleteUserByEmailDao = async (email) => {
    const userDeleted = await User.findOneAndDelete({ userEmail: email })
    if (userDeleted) {
        console.log("Deleted one user")
        return userDeleted
    }
};

/**
 * It updates the user with the given id.
 * @param id - The id of the user to be updated
 * @param userTobeUpdated - This is the user object that is passed in the request body.
 * @returns The user is being returned.
 */
const updateUser = async (id, userToBeUpdated) => {
    const user = User.findByIdAndUpdate({ _id: id }, {
        userPassword: userToBeUpdated.userPassword,
        userName: userToBeUpdated.userName,
        userLastName: userToBeUpdated.userLastName,
        userRole: userToBeUpdated.userRole,
        userEmail: userToBeUpdated.userEmail,
        image: userToBeUpdated.image
    })
    return user;
}

/**
 * The function getAll() is an async function that returns a promise. The promise is resolved with the
 * value of the users variable. The users variable is assigned the value of the result of the find()
 * method of the User model
 */
const getAll = async () => {
    const users = await User.find();
    console.log("Get all Users")
    return users;
};

/**
 * The above function is used to get one user by id.
 * @param id - The id of the user you want to get.
 * @returns The user object
 */
const getOneById = async (id) => {
    const getOneUser = await User.findById({ _id: id })
    if (getOneUser) {
        console.log("Get one user")
        return getOneUser;
    }
};

module.exports = {
    createUser,
    deleteUserByEmailDao,
    deleteUser,
    updateUser,
    getAll,
    getOneById
}