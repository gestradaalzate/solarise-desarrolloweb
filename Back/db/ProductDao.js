const Product= require('../models/productModel')

/**
 * It creates a new product and saves it to the database
 * @param productToBeCreate - This is the object that contains the product details that will be saved
 * in the database.
 * @returns The productSaved is being returned.
 */
const createProduct= async (productToBeCreated)=>{
  const productSaved= new Product({
                      productName: productToBeCreated.productName,
                      productPrice: productToBeCreated.productPrice,
                      userId: productToBeCreated.userId,
                      productAvaiability:productToBeCreated.productAvaiability,
                      productStock:productToBeCreated.productStock,
                      productDescription: productToBeCreated.productDescription,
                      image: productToBeCreated.image})
                      .save()  
  return productSaved;
}

/**
 * It finds a product by its id, and if it exists, it deletes it
 * @param id - The id of the product to be deleted.
 * @returns The productDeleted is being returned.
 */
const deleteProduct= async(id)=>{
  const productDeleted= await Product.findById({_id:id})
  if(productDeleted){
    console.log("Deleted one Product")
  return productDeleted.remove()}
}; 

/**
 * This function takes in the id of the product to be updated and the productToBeUpdated object and
 * updates the product with the new values
 * @param id - The id of the product to be updated.
 * @param productToBeUpdated - This is the object that contains the updated product information.
 * @returns The updated product is being returned.
 */
const updateProduct= async(id, productToBeUpdated)=>{ 
  const product= await Product.findByIdAndUpdate({_id:id},{
                  productName: productToBeUpdated.productName,
                  productPrice: productToBeUpdated.productPrice,
                  userId: productToBeUpdated.userId,
                  productAvaiability:productToBeUpdated.productAvaiability,
                  productStock:productToBeUpdated.productStock,
                  productDescription: productToBeUpdated.productDescription,
                  image: productToBeUpdated.image })
  return product;
};
  
/**
 * The function getAllProducts is an async function that returns a promise that resolves to an array of
 * all the products in the database
 */
const getAllProducts= async()=>{
    const products= await Product.find();
    console.log("Get all Products")
    return products;
}; 

/**
 * This function is used to get one product by its id
 * @param id - The id of the product you want to get.
 * @returns The product with the id that was passed in.
 */
const getOneProductById=async(id)=>{
    const getOneProduct= await Product.findById({_id:id})
    if(getOneProduct){
    console.log("Get one Product")
    return getOneProduct;}
}; 

const getOneProductByName=async(productName)=>{
  const getOneProduct= await Product.findOne({productName:productName})
  if(getOneProduct){
  console.log("Get one Product")
  return getOneProduct;}
}; 
  
module.exports={
  createProduct,
  getOneProductById,
  deleteProduct,
  getAllProducts,
  updateProduct,
  getOneProductByName}