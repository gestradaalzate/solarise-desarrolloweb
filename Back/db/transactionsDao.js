const Transaction = require('../models/transactionModel')

const createTransaction = async (transactionToBeCreate) => {
  const transactionNew = new Transaction({
    address:transactionToBeCreate.address,
    city:transactionToBeCreate.city,
    numberPhone:transactionToBeCreate.numberPhone,
    state:transactionToBeCreate.numberPhone,
    totalPrice:transactionToBeCreate.totalPrice,
    userId:transactionToBeCreate.userId
  }).save()
  return transactionNew;
}

const deleteTransaction = async (id) => {
  const transactionDeleted = await Transaction.findById({ _id: id })
  if (transactionDeleted) {
    console.log("Deleted one Transaction")
    return transactionDeleted.remove()
  }
};

const updateTransaction = async (id, transactionToBeUpdated) => {
  const transaction = Transaction.findByIdAndUpdate({ _id: id }, {
  address:transactionToBeUpdated.address,
  city:transactionToBeUpdated.city,
  numberPhone:transactionToBeUpdated.numberPhone,
  state:transactionToBeUpdated.numberPhone,
  totalPrice:transactionToBeUpdated.totalPrice,
  userId:transactionToBeUpdated.userId
  })
  return transaction;
}

const getAllTransactions = async () => {
  const transactions = await Transaction.find();
  console.log("Get all Transactions")
  return transactions;
};

const getOneTransactionById = async (id) => {
  const getOneTransaction = await Transaction.findById({ _id: id })
  if (getOneTransaction) {
    console.log("Get one Transaction")
    return getOneTransaction;
  }
};

module.exports = {
  createTransaction,
  getOneTransactionById,
  deleteTransaction,
  getAllTransactions,
  updateTransaction
}