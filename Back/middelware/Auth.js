const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const User = require('../models/userModel');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

/* This is a passport strategy that is used to authenticate the user. */
passport.use("login", new localStrategy({
    usernameField: 'name',
    passwordField: 'password'
},
    async (name, password, done) => {
        const user = await User.find({ userName: name })
        console.log("🚀 ~ file: Auth.js ~ line 14 ~ user", user)
        const isValid = await (user[0]).isValidPassword(password)
        if(!isValid){
          return done("not Correct Password", null, { message: 'Not correct password' })
        }
        const newUser = user.map(({userName, userEmail, userRole}) => ({ userName, userEmail, userRole }));
        return done(null, newUser, { message: 'Logged in Successfully' })
    })
);

/* This is a passport strategy that is used to authenticate the user. */
passport.use(
    new JWTstrategy(
      {
        secretOrKey: 'TOP_SECRET_PASSWORD_LETTER_JUST_TEST',
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
      },
      async (token, done) => {
        try {
          return done(null, token.user);
        } catch (error) {
          done(error);
        }
      }
    )
  );
