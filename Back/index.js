const express = require('express')
const app = express()
const {connectDatabase}=require("./config/database");
const port = 3000
const dotenv=require("dotenv").config({path:'./config/secret.config.env'});
const {routesHandler} = require('./routes/routesHandler');
var cors = require('cors')
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(cors())
app.use(express.json())
routesHandler(app);
connectDatabase();

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})