import React from 'react'
import {buttomLogic} from './Buttom'


const Butoom = ({textButton, funtionToHandlen, buttonType, shoudBeDisable, icon}) => {
  const {handleAdd}= buttomLogic(funtionToHandlen)
  return (
    <button 
    className={`btn btn-${buttonType} me-1`}
     disabled={shoudBeDisable}
      onClick={handleAdd}>
      <span className={icon}></span>
      {textButton}
      </button>  
  )
}

export default Butoom