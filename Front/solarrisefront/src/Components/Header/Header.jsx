import React from 'react'
import { Link } from "react-router-dom";
import LoginLogout from '../LoginLogout'
import { isToken } from '../../apiRequest/fetch';
const Header = () => {
  const isTokenGiven = isToken().isToken
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid container">
      <Link className="navbar-brand" to={`/`} ><h3>SolaRise</h3></Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link" to={isTokenGiven?`/products`:`/`} > <span className='bi-box me-2'></span>Products</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={isTokenGiven?`/users`:`/`} > <span className='bi-people me-1'></span>Users</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={isTokenGiven?`/users/create`:`/`} >  <span className='bi-person-plus'></span>Create Users</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={isTokenGiven?`/products/create`:`/`} > <span className='bi-bag-plus me-2'></span>Create Products</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={isTokenGiven?`/transactions`:`/`} > <span className='bi-bar-chart me-2'></span>Transactions</Link>
            </li>
          </ul>
          <LoginLogout></LoginLogout>
        </div>
      </div>
    </nav >

  )
}

export default Header