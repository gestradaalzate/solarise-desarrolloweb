import React from 'react'
import { Link } from "react-router-dom";
import { isToken } from '../../apiRequest/fetch';
import { useDispatch } from 'react-redux';
import { remove_token } from '../../Reducers/UserAccions';
const LoginLogout = () => {
    const dispatch = useDispatch()
    const rand = 1 + Math.random() * (30 - 1);
    const isTokenGiven = isToken()

    return (
        <div> {(isTokenGiven.isToken) ?
            
            <div className='d-flex justify-content-center '>
                <div className="position-absolute bottom-0 end-0 rounded-circle m-auto"
                    style={{ bottom: "5px", margin: "0", width: "50px", "position": "relative" }}>
                    <Link style={{ position: "absolute"}} className=' btn  btn-outline-dark  bi-cart4 mt-4' to={isTokenGiven?`/shopingcar`:`/`} > </Link>
                </div>
                <div
                    className="d-flex mx-3  rounded-circle overflow-hidden  justify-content-center align-items-center">
                    <button className="btn m-1">
                        <img src={`https://robohash.org/${rand}.png?set=set4`} height="45" width="40" />
                    </button>
                </div>
                <div>
                    <span className="name mt-4 text-white ">{isTokenGiven.userInfo.userName}</span>
                    <br></br>
                    <span className="name mt-4 text-white ">Role: {isTokenGiven.userInfo.userRole}</span>
                    <br></br>
                    <Link className="btn btn-outline-danger mt-1 btn-sm" to={`/`} onClick={dispatch(remove_token)} type="submit">
                        <span className='bi-exclamation-circle me-2'></span>Logout</Link>
                </div>
            </div> : <div> <Link className="btn btn-outline-success" to={`/login`} type="submit"><span className='bi-person-circle me-2'></span>Login</Link></div>}
        </div>
    )
}

export default LoginLogout