import React from 'react'
import { useState, useEffect } from 'react'
import Card from './Cards/Card'
import { useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { getAllRequestObjects } from '../apiRequest/getAllRequest';

const ObjectList = ({ ObjectToMakeTheRequest }) => {

    /* Getting the state of the userReducer. */
    var state = useSelector(state => state.userReducer)

    /* Creating a function called useGetAllRequest that will make a request to the server. */
    const useGetAllRequest = getAllRequestObjects()

    /* Making a request to the server to get all the objects of the type ObjectToMakeTheRequest. */
    useEffect(() => {
        useGetAllRequest(ObjectToMakeTheRequest + "/all")
            .then(response => response.json())
            .then(setStateList)
    }, [ObjectToMakeTheRequest]);

    /* Creating a state called list and a function to change the state called setStateList. */
    const [list, setStateList] = useState([]);

    /* Checking if the pathname includes the word users. If it does, it will return the string 'see user
    details' and if it doesn't it will return the string 'Buy this product'. */
    const textButton = () => {
        if (useLocation().pathname.includes('users')) { return 'See user details' }
        else if (useLocation().pathname.includes('products')) { return 'Buy this product' }
        else { return 'see transaction'}
    }

    return (
        <div className='container p-4'>
            <h1>List of {ObjectToMakeTheRequest}</h1>
            <div className=" row">
                {
                    list.map((object) =>
                        <Card
                            userImage={object.image}
                            userName={object.userName}
                            userLastName={object.userLastName}
                            userEmail={object.userEmail}
                            objectName={object.productName}
                            objectPrice={object.productPrice}
                            objectStock={object.productStock}
                            objectImage={object.image}
                            key={object._id}
                            textButton={textButton()}
                            trasactionDate={object.date}
                            totalPrice={object.totalPrice}>
                        </Card>
                    )
                }
            </div>
        </div>
    )
}

export default ObjectList