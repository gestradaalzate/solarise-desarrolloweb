import React from 'react'
import Header from '../Header/Header'
import CreateUserForm from '../Forms/CreateUserForm'
import CreateProductForm from './CreateProductForm';

const ObjectsForm = ({ contentType }) => {
  return (
    <>
      <Header></Header>
      {(contentType.includes('users'))?(<CreateUserForm />) : (<CreateProductForm />)}
    </>
  )
}

export default ObjectsForm