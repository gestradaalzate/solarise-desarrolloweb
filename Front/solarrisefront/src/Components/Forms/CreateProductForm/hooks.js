import { useState } from 'react';
import { useCreateProduct } from '../../../apiRequest/createProductRequest';

export const CreateProductFormLogic = () => {

    /* Calling the useCreateProduct hook, which is imported from the apiRequest/createProductRequest.js
    file. */
    const createProduct = useCreateProduct()

    /* Getting the state of the userReducer from the redux store. */
    //var state = useSelector(state => state.userReducer)

    /* Setting the state of the component to an object with the properties of productName,
    productPrice, userId, productAvaiability, productStock, and productDescription. */
    const [object, setState] = useState({
        productName: '',
        productPrice: '',
        userId: '',
        productStock: '',
        productDescription: '',
        productAvaiability:false
    });

    /**
     * The function takes an event as an argument, and then it prevents the default action of the event,
     * and then it takes the name and value of the event target, and then it sets the state of the
     * component to the previous state, and then it returns the previous state with the name and value
     * of the event target
     */
    const handleChange = event => {
        const { name, value, type, checked } = event.target;
        setState(prevState => ({
            ...prevState,
            [name]: type === "checkbox" ? checked : value
        }));
        console.log('value is:', object);
    };

    /**
     * The function is called when the user clicks the submit button on the form. It prevents the
     * default action of the form, which is to reload the page, and then it calls the
     * createProductRequest function, which is imported from the productActions.js file
     */
    const onSubmitHandler = event => {
        event.preventDefault()
        console.log("Product to be create: ", object)
        createProduct(object)
    }

    /* Returning the onSubmitHandler, handleChange, and object variables. */
    const res = {
        onSubmitHandler,
        handleChange,
        object
    }

    /* Returning the onSubmitHandler, handleChange, and object variables. */
    return res
}