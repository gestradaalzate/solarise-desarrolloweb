import React from 'react'
import Input from '../../Inputs/Input'
import { CreateProductFormLogic } from './hooks'
import { alertProduct } from '../alert'


const CreateProductForm = () => {

    const { onSubmitHandler, handleChange, object } = CreateProductFormLogic()

    return (
        <>
            <div className='container mt-4'>
                <h1 className='container mb-4'>Create Product</h1>
                <form onSubmit={onSubmitHandler}>
                    <Input
                        title="Image → URL"
                        info="Insert the url of the product image."
                        type="text"
                        id="image"
                        handleChange={handleChange}
                        value={object.image}>
                    </Input>
                    <Input title="Name"
                        info="Input the name of the product."
                        type="text"
                        id="productName"
                        handleChange={handleChange}
                        value={object.productName}>

                    </Input>
                    <Input
                        title="Price"
                        info="Insert how much is the price of the product."
                        type="text"
                        id="productPrice"
                        handleChange={handleChange}
                        value={object.productPrice}>
                    </Input>
                    <Input
                        title="Stock"
                        info="Input the amount of the product in stock."
                        type="text"
                        id="productStock"
                        handleChange={handleChange}
                        value={object.productStock}>
                    </Input>
                    <Input
                        title="User ID"
                        info="Input your ID."
                        type="text"
                        id="userId"
                        handleChange={handleChange}
                        value={object.userId}>
                    </Input>
                    <Input
                        title="Description"
                        info="Provide a short description of the product."
                        type="text"
                        id="productDescription"
                        handleChange={handleChange}
                        value={object.productDescription}>
                    </Input>
                    <div className="mb-3">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            onChange={handleChange}
                            checked={object.productAvaiability}
                            name="productAvaiability"
                            value={object.productAvaiability}
                            id="flexCheckDefault" />
                        <div id="productAvaiabilityHelp" className="form-text">Product Availability</div>
                    </div>
                    <button onClick={() => alertProduct()} type="submit" className="btn btn-primary" disabled={!object.image || !object.productName || !object.productPrice || !object.productStock || !object.userId}>Submit</button>
                </form>
            </div>
        </>
    )
}

export default CreateProductForm