import { useState, useDebugValue } from 'react'
import { useCreateUser } from '../../../apiRequest/createUserRequest';

export const useCreateUserForm = () => {

    const createUser = useCreateUser()

    const [object, setState] = useState({
        userName: '',
        userLastName: '',
        userPassword: '',
        userRole: '',
        userEmail: ''
    });

    const handleChange = event => {
        event.preventDefault();
        const { name, value } = event.target;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };


    const onSubmitHandler = event => {
        event.preventDefault()
        console.log("user to be create: ", object)
        createUser(object)
    }

    const res = {
        object,
        handleChange,
        onSubmitHandler,
        canCreateUser: Boolean(createUser),
    }

    useDebugValue(res)

    return res
}
