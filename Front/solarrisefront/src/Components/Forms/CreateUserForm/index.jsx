import React from 'react'
import Input from '../../Inputs/Input'
import { useCreateUserForm } from './hooks'
import { alertUser } from '../alert'

const CreateUserForm = () => {

    const { object, handleChange, onSubmitHandler, canCreateUser } = useCreateUserForm()

    return (
        <div className='container mt-4'>
            <h1 className='container mb-4'>Create User</h1>
            <form onSubmit={onSubmitHandler}>
                <Input
                    title="Photo"
                    info="We'll never share your image with anyone else"
                    type="text"
                    id="image"
                    handleChange={handleChange}
                    name={object.image}>
                </Input>
                <Input title="First Name"
                    info="We'll never share your first name with anyone else."
                    type="text"
                    id="userName"
                    handleChange={handleChange}
                    name={object.userName}>
                </Input>
                <Input
                    title="Last Name"
                    info="We'll never share your last name with anyone else."
                    type="text"
                    id="userLastName"
                    handleChange={handleChange}
                    name={object.userLastName}>
                </Input>
                <Input
                    title="Role"
                    info="We'll never share your role with anyone else."
                    type="text"
                    id="userRole"
                    handleChange={handleChange}
                    name={object.userRole}>
                </Input>
                <Input
                    title="Password"
                    info="We'll never share your password with anyone else."
                    type="password"
                    id="userPassword"
                    handleChange={handleChange}
                    name={object.userPassword}>
                </Input>
                <Input
                    title="Email"
                    info="We'll never share your email with anyone else."
                    type="email"
                    id="userEmail"
                    handleChange={handleChange}
                    name={object.userEmail}>
                </Input>
                <button onClick={() => alertUser()} disabled={!object.userEmail || !object.userName || !object.userPassword || !object.userRole || !canCreateUser} type="submit" className="btn btn-primary" >Submit</button>

            </form>
        </div>

    )
}

export default CreateUserForm