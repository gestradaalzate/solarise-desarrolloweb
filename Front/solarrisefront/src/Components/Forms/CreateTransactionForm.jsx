import React from 'react'
import Input from '../Inputs/Input'
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import {alert} from './alert'
import { useFetch } from '../../apiRequest/fetch';
const CreateTransactionForm = () => {
  
  var state = useSelector(state => state.shoppingCarReducer)

  const [object, setState] = useState({
    address: '',
    city: '',
    numberPhone: '', 
    state: '',
    userId: '',
    totalPrice: ''
  });

  const handleChange =   event => {
    event.preventDefault()
    const { name, value } = event.target;
    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
    console.log("object",object.userId)
  };

  const {post} = useFetch();
  const onSubmitHandler = event => {
    event.preventDefault()
    const createUser = (object) => post('transactions/', object).then(res => console.log(res))
    return createUser(object);
  }

  return (
    <>
      <div className='container mt-4'>
        <h1 className='container mb-4'>Transaction</h1>
        <form onSubmit={onSubmitHandler}>

          <Input title="Address"
            info="* Input your address"
            type="text"
            id="address"
            handleChange={handleChange}
            value={object.address}>
          </Input>

          <Input title="City"
            info="* Input your city "
            type="text"
            id="city"
            handleChange={handleChange}
            value={object.city}>
          </Input>

          <Input title="Phone number"
            info="* Input your phoneNumber"
            type="text"
            id="numberPhone"
            handleChange={handleChange}
            value={object.numberPhone}></Input>

          <Input title="State"
            info="* Input your state"
            type="text"
            id="state"
            handleChange={handleChange}
            value={object.state}>
          </Input>

          <Input
            title="User ID"
            info="* Input your ID"
            type="text"
            id="userId"
            handleChange={handleChange}
            value={object.userId}>
          </Input>
          <div className='m-2'>
              <span className="fw-bolder">Total Price: </span>{state.totalPrice}
          </div>
          <button onClick={()=>alert()} disabled={!object.address|| !object.city||!object.numberPhone||!object.state||!object.userId }type="submit" className="btn btn-primary m-2">Submit</button>
         
          <Link className="btn btn-danger m-2" to={"/"} role="button">Return</Link>
        
        </form>

      </div>
    </>
  )
}

export default CreateTransactionForm