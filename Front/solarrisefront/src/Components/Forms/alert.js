import swal from 'sweetalert';
import CreateTransactionForm from './CreateTransactionForm';

export const alert=()=>{
  if(CreateTransactionForm){
  swal({
  title:"Do you want to confirm your purchase?",
  icon:"warning",
  buttons:["No","Yes"]
  }).then(respuesta=>{
    if(respuesta){
      swal({text:"Transaction carried out successfully!",icon:"success",timer:"3000"})
    }
  })}
}
export const alertProduct=()=>{
  swal({
  title:"Do you want to confirm the creation of this product?",
  icon:"warning",
  buttons:["No","Yes"]
  }).then(respuesta=>{
    if(respuesta){
      swal({text:"Product created successfully!",icon:"success",timer:"3000"})
    }
  })}
  
export const alertUser=()=>{
    swal({
    title:"Do you want to confirm the creation of this user?",
    icon:"warning",
    buttons:["No","Yes"]
    }).then(respuesta=>{
      if(respuesta){
        swal({text:"User created successfully!",
        icon:"success",timer:"3000"})
      }
    })
  }