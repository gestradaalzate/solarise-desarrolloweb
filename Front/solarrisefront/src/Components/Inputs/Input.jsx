import React from 'react'

const Input = ({title, info, type, id, handleChange, value}) => {

    return (
        <div >
                <div className="mb-3">
                    <input 
                    type={type} 
                    className="form-control" 
                    id={id+"id"}  
                    name={id}
                    placeholder={title} 
                    aria-describedby="emailHelp"
                    onChange={handleChange}
                    value={value}/>
                <div id="emailHelp" className="form-text">{info}</div>
                </div>
        </div>
    )
}

export default Input