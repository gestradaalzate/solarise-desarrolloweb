import React, { useState } from 'react'
import Butoom from '../Buttons/Butoom'
import { add_function } from '../../Reducers/ShoppingCarAccions';
import { v4 as uuid } from 'uuid';
import { useDeleteUser } from '../../apiRequest/createUserRequest';
import { useDispatch } from 'react-redux';
import { isToken } from '../../apiRequest/fetch';
const Card = ({ userImage, 
                userName, 
                userLastName,
                userEmail,  
                objectName, 
                objectPrice, 
                objectStock, 
                objectImage, 
                textButton,
                trasactionDate,
                totalPrice }) => {
    const [number, setState] = useState(0)

    const id = uuid();

    const dispatch = useDispatch();

    const add = add_function()

    const deleteUser = useDeleteUser()
    /**
     * The function Productslabel is a function that returns a div with a h5 and a p tag
     */
    const Productslabel = () => {
        
        return (<div>
            <div>
            <img src={objectImage} className='card-img-top mb-3 shadow  rounded' width={250} height={350} style={{objectFit: 'fill'}}></img> 
                <h4 className="card-title">{objectName}</h4>
                <p className="card-text"><b>Price: $</b>{objectPrice}<br/>
                <b>Availability:</b> {objectStock - number} units.</p><br/>
                 
            </div>
            <Butoom
                textButton={textButton}
                buttonType="primary"
                funtionToHandlen={
                    addAndProofProductAvailability
                }
                icon={(objectName != null) ? "bi-bag me-2" : "bi-eye me-1"}
                shoudBeDisable={!isAvaible} />
        </div>)
    }

    const TrasactionLabel = () => {
        const isTokenGiven = isToken()
        return (isTokenGiven.userInfo.userRole==='ADMIN')?(<div>
            <div>
                <h4 className="card-title">{objectName}</h4>
                <p className="card-text"><b>trasaction Date: </b>{trasactionDate}<br/>
                <b>Price:</b> {totalPrice} </p><br/>
                 
            </div>
            <Butoom
                textButton={textButton}
                buttonType="primary"
                funtionToHandlen={
                    addAndProofProductAvailability
                }
                icon={(objectName != null) ? "bi-bag me-2" : "bi-eye me-1"}
                shoudBeDisable={!isAvaible} />
        </div>):<>You are not ADMIN </>
    }

    /**
     * It returns a div with a h5 and a p tag
     * @returns A React component
     */
    const Userlabel = () => {
        const isTokenGiven = isToken()
        return (<div className=''>
            <img src={userImage} className='card-img-top mb-3 shadow rounded' width={250} height={350} style={{objectFit: 'fill'}}/>
            <h4 className="card-title">{userName + " " + userLastName}</h4>
            <p className="card-text">{userEmail}</p>
            <Butoom
                textButton={textButton}
                buttonType="primary"
                funtionToHandlen={
                    addAndProofProductAvailability
                }
                icon={(objectName != null) ? "bi-bag me-2" : "bi-eye me-1"}
                shoudBeDisable={false} />
            {(isTokenGiven.userInfo.userRole==='ADMIN')?<Butoom
                textButton="Delete"
                buttonType="danger"
                funtionToHandlen={
                    () => deleteUser(userEmail)
                } />:<></>}
        </div>
        )
    }

    /* Creating an object with the information of the product to add to the shopping cart */
    const object = {
        productId: id,
        productName: objectName,
        price: objectPrice
    }

    const isAvaible = (objectStock - number > 0)
    const addAndProofProductAvailability = () => {
        setState(number + 1)
        if (isAvaible) {
            dispatch(add(object))
        }
        else {
            console.log("ZERO:::: ");
        }

    }
    const labelSelector = ()=>{
        if(objectName != null){
            return <Productslabel />
        }
        else if(trasactionDate != null) {
            return  <TrasactionLabel />
        }
        else{
            return<Userlabel />
        }
    }
    return (
        <div className="card col-12 me-auto p-2 m-2 shadow p-3 mb-5 bg-white rounded" style={{ width: "26rem" }}>

            <div className="card-body">
                {labelSelector() }
            </div>
        </div>
    )
}

export default Card