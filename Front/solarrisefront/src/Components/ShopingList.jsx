import React from 'react'
import Butoom from './Buttons/Butoom'
import { remove_function } from '../Reducers/ShoppingCarAccions'
import { useDispatch } from 'react-redux';

const ShopingList = ({ state }) => {
    const userItem = remove_function()
    const dispatch = useDispatch();
    return (
        <div>
            {
                !(state.items === undefined || state.items.length == 1) ?
                    <div> <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Producto Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {state.items.map((item) => (item.productName !== '') ?
                                <tr key={item.productId}>
                                    <th scope="row">-</th>
                                    <td>{item.productName}</td>
                                    <td>{item.price}</td>
                                    <td>
                                        <Butoom className='btn btn-warning'
                                            buttonType="warning"
                                            textButton="Remove"
                                            icon={"bi-trash me-1"}
                                            funtionToHandlen={() => dispatch(userItem(item))}/>
                                    </td>
                                </tr> : null)}
                        </tbody>
                    </table>
                    </div> : <div></div>
            }
        </div>
    )
}

export default ShopingList