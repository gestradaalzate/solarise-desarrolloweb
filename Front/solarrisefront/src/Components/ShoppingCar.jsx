import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { action_fetch } from '../Reducers/ShoppingCarAccions';
import ShopingList from './ShopingList';
const ShoppingCar = () => {

    var state = useSelector(state => state.shoppingCarReducer)

    useEffect(() => {
        totalPrice()
    }, [])

    const cleanList = state.items.filter(
         function (item) { 
            return item.price !== undefined } 
        );

    const totalPrice = () => {
        const listPrice = cleanList.map((item) => Number(item.price))
        if (listPrice.length > 0) {
            const totalPrice = listPrice.reduce((preV, currV) =>  preV + currV ,0);
            return totalPrice;
        } else {
            return 0
        }
    }

    return (
        <>
            <div className='container p-4'>
                <h1>Shopping Cart</h1>
                <div>{state.userName}</div>
                <ShopingList state={state}></ShopingList>
                <div className='m-2'>
                    <span className="fw-bolder">Total Price: </span>{totalPrice()}</div>
                <Link to='/transactions/create'>
                <button type="button" className='btn btn-primary'><span className='bi-check2-circle me-2'/>Make Transaction</button>
                </Link>

            </div>

        </>
    )
}

export default ShoppingCar