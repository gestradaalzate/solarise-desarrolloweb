import { useFetch } from "./fetch"

/**
 * It returns a function that takes an object and posts it to the products endpoint
 * @returns A function that takes an object as an argument and returns a promise.
 */
export const useCreateProduct = () => {
    const { post } = useFetch();
    return post && ((object) => post("products/", object))
}

