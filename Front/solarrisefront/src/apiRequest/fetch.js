import { useSelector } from 'react-redux'
import { config } from './Url'
// process.env.NODE_ENV_BASE_URL
const baseUrl = config.url.API_URL + '/api/v1/'
/**
 * It takes a method (GET, POST, etc.) and returns a function that takes a url and a body and returns a
 * fetch request with the appropriate headers
 * @returns An object with get, post, delete, and update functions.
 */
export const useFetch = () => {
    /* Using the `useSelector` hook to get the token from the Redux store. */
    const token = useSelector(state => state.userReducer.token)
    /**
     * It takes a method (GET, POST, etc.) and returns a function that takes a url and a body and
     * returns a fetch request with the appropriate headers
     * @param method - The HTTP method to use.
     */
    const f = (method) => (url, body) => fetch(baseUrl + url, {
        method,
        body: body && JSON.stringify(body),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
    })
    /* Returning an empty object if there is no token, and an object with get and post functions if
    there is a token. */
    if (!token) return {}
    else
        return {
            get: f('GET'),
            post: f('POST'),
            deleteUser: f('DELETE'),
            update: f('UPDATE'),
        }
}

export const isToken = () => {
    /* Using the `useSelector` hook to get the token from the Redux store. */
    const token = useSelector(state => state.userReducer.token)

    if (!token) { return''; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    const infoToken = JSON.parse(window.atob(base64));
    const res = {
        isToken: (token) ? true : false,
        userInfo: infoToken.user[0],
    }
    return res
}