import { useFetch } from "./fetch";

/**
 * It returns a function that takes an object and posts it to the users endpoint
 * @returns A function that takes an object as an argument and returns a promise.
 */
export const useCreateUser = () => {
    const { post } = useFetch()
    return post && ((object) => post('users/', object))
}
export const useDeleteUser = () => {
    const { deleteUser } = useFetch()
    return deleteUser && ((object) =>  deleteUser(`users/email/${object}`,{}))
}

export const useUpdateUser = () => {
    const { update } = useFetch()
    return update && ((object) => update('users/', object))
}
export const useGetUser = () => {
    const { get } = useFetch()
    return get && ((object) => get('users/', object))
}
