import { useFetch } from "./fetch"

/**
 * It returns a function that takes an object and returns a promise
 * @returns A function that takes an object and returns a promise.
 */
export const getAllRequestObjects = () => {
    const api = useFetch();
    return (ObjectToMakeTheRequest) => api.get(ObjectToMakeTheRequest,null);
}
