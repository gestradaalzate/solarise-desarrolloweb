import { config } from "./Url";
/**
 * It takes in a userDetails object, and returns a promise that resolves to a JSON object
 * @param userDetails - This is the object that contains the username and password.
 * @returns A promise that resolves to a JSON object.
 */
export const login = async (userDetails) => {
    return await fetch(config.url.API_URL + '/login', {
        method: 'POST',
        body: JSON.stringify(userDetails),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((response) => { return response.json(); })
        .catch((error) => {
            console.log("ERROR");
            return "error"
        })
}