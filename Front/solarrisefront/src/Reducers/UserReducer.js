import {initialStateUser} from "./States";
 
/**
 * The userReducer function takes in a state and an action and returns a new state
 * @param [state] - This is the current state of the store.
 * @param action - This is the action object that was returned from the action creator.
 * @returns The token is being returned.
 */
const userReducer = (state=initialStateUser, action) =>{
    switch (action.type) {
        case 'login':
          return { token: action.payload.token };
        case 'logout':
          return { token: '' };
        default:
          return state;
      }
}

export default userReducer;