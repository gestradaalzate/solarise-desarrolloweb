/**
 * It returns an object with a type property and a payload property. The type property is a string that
 * says "ADD" and the payload property is an object that has an item property that is an object with a
 * productName property that is equal to the objectName parameter
 * @param object - The name of the object you want to add to the array.
 */
export const add_function = () => (object) => {
        return {
            type: 'ADD',
            payload: {
                item: object
            }
        }
    }

export const remove_function=()=> (object) => {
    return {
        type: 'REMOVE',
        payload: {
            item: object
        }
    }
}

/* Exporting an action that is called action_fetch. */
export const action_fetch = {
    type: 'FETCH_STATE',
    payload: {
    }
};
