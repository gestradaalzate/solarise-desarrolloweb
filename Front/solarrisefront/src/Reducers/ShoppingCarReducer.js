import { initialStateShoppingCar } from "./States";

/**
 * It returns a new state object with the same userName and a new items array that includes the new
 * item
 * @param [state] - The current state of the store.
 * @param action - The action object that was dispatched.
 */
const shoppingCarReducer = (state = initialStateShoppingCar, action) => {
  
  switch (action.type) {
    case 'ADD':
      console.log("action.type: ADD", state, "accion", action)
      return {
        userName: state.userName,
        items: [...state.items, action.payload.item],
        totalPrice: state.totalPrice + action.payload.item.price,
      };
    case 'REMOVE':
      console.log("action.type: REMOVE", state, "accion", action)
      return {
        userName: state.userName,
        items: state.items.filter(item => item.productId !== action.payload.item.productId),
        totalPrice: state.totalPrice - action.payload.item.price,
      };
    default:
      return state;
  }
};



export default shoppingCarReducer