/* Setting the initial state of the application. */
export const initialStateShoppingCar = {
  userName: '',
  items: [
    { productName: '',
      price: ''
    }
  ],
  totalPrice: 0,
};

/* Setting the initial state of the application. */
export const initialStateUser = {
  token:''
}
