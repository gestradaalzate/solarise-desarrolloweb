/**
 * It returns an object with a type of 'login' and a payload of an object with a token property
 * @param tokenToAdded - The token that will be added to the store.
 * @returns An object with a type and a payload.
 */
export function add_token(tokenToAdded) {
    return {
        type: 'login',
        payload: {
            token: tokenToAdded,
        }
    }
}

 export function remove_token() {
    console.log("User Accions remove_token")
    return {
        type: 'logout',
        payload: {
        }
    }
}

/**
 * It returns an object with a type and a payload
 * @returns An object with a type and a payload.
 */
export function get_token() {
    return {
        type: 'FETCH_STATE',
        payload: {
        }
    }
}