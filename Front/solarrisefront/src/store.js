import { createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools} from 'redux-devtools-extension';
import shoppingCarReducer from './Reducers/ShoppingCarReducer';
import userReducer from './Reducers/UserReducer';

/* Combining the reducers. */
const reducers= combineReducers ({
    shoppingCarReducer:shoppingCarReducer,
    userReducer:userReducer
    })
  
/* It's creating an array with the middleware we want to use. */
const middleware= [thunk]

/**
 * We're creating a store with our reducers, and we're applying our middleware to the store
 * @returns A store with the reducers and middleware
 */
function configureStore() {
    return createStore (reducers,composeWithDevTools(applyMiddleware(...middleware)))
}
export default configureStore;