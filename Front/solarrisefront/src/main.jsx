import React from 'react'
import ReactDOM from 'react-dom/client'
import {RouterProvider} from "react-router-dom";
import router from "./Routes/Router";
import { Provider } from 'react-redux';
import configureStore from './store';
import { Link } from "react-router-dom";

var store = configureStore()

ReactDOM.createRoot(document.getElementById('root')).render(
  /* A way to pass the store to the components. */
  <Provider store={store}>
    <RouterProvider router={router} />

  </Provider>
)
