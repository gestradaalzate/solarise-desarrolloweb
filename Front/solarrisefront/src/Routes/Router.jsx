import {
    createBrowserRouter,
} from "react-router-dom";
import { useFetch, isToken } from "../apiRequest/fetch";
import CreateTransactionForm from "../Components/Forms/CreateTransactionForm";
import ObjectsForm from "../Components/Forms/ObjectsForm";
import Home from "../View/Home";
import ListView from "../View/ListView";
import Login from "../View/Login";
import ShopingCarView from "../View/ShopingCarView";

/* Creating a router for the application. */
 // Add your own authentication on the below line.

const router = createBrowserRouter([
    {
        path: "/",
        element: <Home />
    },
    {
        path: "/users",
        element: <ListView contentType="users"/>,
    },
    {
        path: "/login",
        element: <Login />,
    },
    {
        path: "/shopingcar",
        element: <ShopingCarView />,
    },
    {
        path: "/products",
        element: <ListView contentType="products"/>,
    },
    {
        path: "/users/create",
        element: <ObjectsForm contentType="users"/>,
    },
    {
        path: "/products/create",
        element: <ObjectsForm contentType="products"/>,
    },
    {
        path: "/transactions",
        element: <ListView contentType="transactions"/>,
    },
    {
        path: "/transactions/create",
        element: <><CreateTransactionForm /></>,
    },

]);

export default router