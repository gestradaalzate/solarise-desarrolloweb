import React from 'react'
import Header from '../Components/Header/Header'
import ShoppingCar from '../Components/ShoppingCar.jsx'

const Home = () => {
    return (
        <div style={{ "background": "#EBF5FB" }}>
            <Header />
            <div className="p-5 text-white text-center"
                style={{
                    "backgroundImage": `url('../src/assets/solaRise.png')`,
                    "height": "86vh",
                    "backgroundSize": "cover",
                    "backgroundRepeat": "no-repeat",
                }}
            >
                <img src="../src/assets/logoNegro.png" align="right" width={550}></img>
            </div>
            <div className="container mt-5">
                <div className="row">

                    <h1 className="mt-4" align="center"> SolaRise is the #1 Home Solar Company in Colombia<br /></h1>

                    <div className="col-sm-4">
                        <h3 className="mt-4"> Hey! Why be average when you can be extraordinary? Go Green Now!</h3><br></br>

                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://img.freepik.com/fotos-premium/instalacion-sistema-paneles-solares-fotovoltaicos-techo-casa_10069-5631.jpg" align="right" width={400}></img></div>

                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://i.pinimg.com/736x/d5/0d/09/d50d090f4a8205afa221c555ca2723a4.jpg" align="right" width={400}></img></div>

                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkCvCZquJn0xPvwKySy2U3s6s6EZvk60c47Fss78XKM3GH1nDwgCpAVt7FVHxneGTMBSc&usqp=CAU"
                            align="right" width={400}></img></div>
                    </div>


                    <div className="col-sm-4">

                        <br /><br /><br /><br /><br />

                        <h3 className="mt-5"> It's Time for Solar </h3>

                        <p>The future is electric. There’s never been a better time to take back control of your energy.</p>

                        <h3 className="mt-5">Solar Benefits</h3>
                        <ul>
                            <li> Get Future Ready</li>
                            <li> Say No To Blackouts</li>
                            <li> Save More</li>
                            <li> Preserve Your Home</li>
                            <li> Boost Your Home Value</li>
                            <li> Control Your Energy</li></ul><br />

                        <p>From powering electric vehicles to working from home, our future is electric. Power outages are increasing nationwide. With battery backup, you can stay plugged in. Furthermore, tell utlitity price hikes to take a hike. Lock in a simple, predictable rate that serves your needs. Also, a future-ready home can be more appealing to more buyers.</p>

                        <h3 className="mt-5">This is why</h3>


                        <p>The sun is at the center of everything. Our world literally revolves around it. It has the power to light up your world, power through storms, drive transportation into the future, and even reduce the cost of energy. From plants to animals, to a child smile on a summer afternoon. Everything in life is powered by the sun. Shouldn't your home be too? <br></br><br></br> <b><i>Start Your Solar Journey Today!</i></b>
                        </p><br></br>
                    </div>



                    <div className="col-sm-4">

                        <h3 className="mt-4"> SolaRise provides clean, renewable energy. Home solar is affordable and emissions-free.</h3><br></br>
                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://cdn.daa.net/images/energia-solar/Beneficios-de-instalar-aire-acondicionado-solar.jpg" align="right" width={400} height={280}></img></div><br></br>

                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://www.greenenergy.com.pe/portal/wp-content/uploads/1.jpg" align="right" width={400}></img></div><br></br>

                        <div className="fakeimg"><img className="rounded card-img-top mb-3 shadow" src="https://g.foolcdn.com/editorial/images/582182/solar-panels-on-home.jpg" align="right" width={400}></img></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home