import React from 'react'
import Header from '../Components/Header/Header'
import ShoppingCar from '../Components/ShoppingCar.jsx'

const ShopingCarView = () => {
  return (
    <div>  <Header />
    <ShoppingCar/></div>
  )
}

export default ShopingCarView