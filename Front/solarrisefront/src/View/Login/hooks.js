import { useState } from 'react'
import { useDispatch } from 'react-redux';
import { add_token } from '../../Reducers/UserAccions';
import { useNavigate } from 'react-router-dom'
import { login } from '../../apiRequest/loginRequest';

export const loginlogic = () => {
    
    /* A hook that allows you to add React state to function components. */
    const [userDetails, setState] = useState({ name: "admin", password: "asd" })

    /* A hook that allows you to navigate to different routes in your application. */
    const navigate = useNavigate()

    /* A hook that allows you to dispatch actions to the Redux store. */
    const dispatch = useDispatch();

    /**
     * The handleChange function takes an event as an argument, prevents the default action, and then
     * sets the state of the component to the name and value of the event target
     */
    const handleChange = event => {
        event.preventDefault();
        const { name, value } = event.target;
        setState((state) => ({
            ...state,
            [name]: value
        }));
    };

    /**
     * A function that is called when the user submits the login form. It prevents the default action of
     * the form submission, calls the login function, and then calls the validateAndRedirect function.
     */
    const onSubmitHandler = async event => {
        event.preventDefault()
        login(userDetails)
            .then((response) => validateAndRedirect(response))
            .then(navigate('/'));
    }

    /**
     * If the response from the server contains a token, add it to the redux store. If not, redirect to
     * the login page
     */
    const validateAndRedirect = (response) => {
        if (!(response.token === null ||
            response.token === "" ||
            response.token === undefined)) {
            dispatch(add_token(response.token));
        } else {
            navigate('/login')
        }
    }

    const res = {
        onSubmitHandler,
        handleChange,
        userDetails
    }
    
    return res
}