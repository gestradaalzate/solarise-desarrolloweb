import React from 'react'
import Input from '../../Components/Inputs/Input'
import Header from '../../Components/Header/Header'
import { loginlogic } from './hooks'


const Login = () => {

  const { userDetails, onSubmitHandler, handleChange } = loginlogic()

  return <>
    <Header />
    <div className="p-5  text-left" 
             style={{ "backgroundImage": `url('https://images.unsplash.com/photo-1557626204-59dd03fd2d31?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHx8&w=1000&q=80')`,
             "height": "180vh",
             "backgroundSize": "cover",
             "backgroundRepeat": "no-repeat",
              }}
            >
    <div className="card col-12 m-6 shadow p-3 bg-white rounded container col-3" style={{ width: "27rem" }} >
     <b><h2><img className= "mx-auto d-block" src="../src/assets/logo.png" width={270} height={100}></img>Sign in</h2></b><br/>

      <form onSubmit={onSubmitHandler}>
     
        <Input
          title="User Name"
          info="Input your user name"
          type="text"
          id="name"
          handleChange={handleChange}
          value={
userDetails.name
}>  
        </Input>

        <Input
          title="Password"
          info="Input your password"
          type="password"
          id="password"
          handleChange={handleChange}
          value={userDetails.password}>
        </Input>
        <button type="submit" className="btn btn-primary">Login</button>
      
      </form>
      </div>
      </div>
  </>
}

export default Login 