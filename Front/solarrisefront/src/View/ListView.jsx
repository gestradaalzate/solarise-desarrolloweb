import React from 'react'
import Header from '../Components/Header/Header'
import { useLocation } from 'react-router-dom'
import ObjectList from '../Components/ObjectList'
const ListView = ({contentType}) => {
    return (
        <>  
            <Header />
            <ObjectList ObjectToMakeTheRequest={contentType}></ObjectList>
        </>
    )
}

export default ListView