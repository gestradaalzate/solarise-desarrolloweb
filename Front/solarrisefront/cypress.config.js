const { defineConfig } = require('cypress');

module.exports = defineConfig({
  component: {
    setupNodeEvents(on, config) {},
    "specPattern": "src/**/*.test.{js,ts,jsx,tsx}"
  },

  "component": {
    "devServer": {
      "framework": "react",
      "bundler": "vite"
    }
  }
});